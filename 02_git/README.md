# Persönliches Portfolio mit GIT und Markdown <!-- omit in toc -->

Für die LB-2 erstellen Sie ein persönliches Portfolio in Form eines GIT-Repositories. In diesem Block bereiten Sie Ihr persönliches Notebook vor und erlernen die Grundlagen von *Markdown* und *GIT*. 

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. Ziele](#1-ziele)
- [2. Eigenes Vorgehen planen - Wie erarbeite ich das Thema Git?](#2-eigenes-vorgehen-planen---wie-erarbeite-ich-das-thema-git)
  - [2.1. Aufgabe](#21-aufgabe)
- [3. Versionsverwaltung mit GIT](#3-versionsverwaltung-mit-git)
  - [3.1. Aufgabenstellung](#31-aufgabenstellung)
  - [3.2. Versionsverwaltung in der Informatik](#32-versionsverwaltung-in-der-informatik)
  - [3.3. Was ist GIT?](#33-was-ist-git)
  - [3.4. Verständnisfragen](#34-verständnisfragen)
- [4. Eigenen Laptop für die Verwendung von GIT vorbereiten](#4-eigenen-laptop-für-die-verwendung-von-git-vorbereiten)
  - [4.1. Anleitung (Windows 10)](#41-anleitung-windows-10)
  - [4.2. Tipps Visual Studio Code](#42-tipps-visual-studio-code)
- [5. Account auf gitlab mit TBZ E-Mail Adresse erstellen](#5-account-auf-gitlab-mit-tbz-e-mail-adresse-erstellen)
  - [5.1. Anleitung](#51-anleitung)
- [6. Neues Repository für persönliches Portfolio erstellen](#6-neues-repository-für-persönliches-portfolio-erstellen)
  - [6.1. Anleitung - Remote first](#61-anleitung---remote-first)
  - [6.2. Aufbau der GitLab Repository Startseite](#62-aufbau-der-gitlab-repository-startseite)
  - [6.3. Repository Strukturierung](#63-repository-strukturierung)
  - [6.4. Anleitung für GitHub](#64-anleitung-für-github)
  - [6.5. Anleitung -GitLab Project Member hinzufügen](#65-anleitung--gitlab-project-member-hinzufügen)
  - [6.6. Grundlagen Installation Visual Studio Code & SSH-Keys](#66-grundlagen-installation-visual-studio-code--ssh-keys)
  - [6.7. Video - Installation von Git & Visual Studio, Neues Projekt erstellen, Projekt klonen](#67-video---installation-von-git--visual-studio-neues-projekt-erstellen-projekt-klonen)
- [7. Anleitungen](#7-anleitungen)
  - [7.1. Git HTTPs durch SSH Connection String ersetzen](#71-git-https-durch-ssh-connection-string-ersetzen)
- [8. Markdown Syntax kennenlernen](#8-markdown-syntax-kennenlernen)
  - [8.1. Aufgabenstellung](#81-aufgabenstellung)
  - [8.2. Links](#82-links)
- [9. Git Branching](#9-git-branching)
  - [9.1. Aufgabenstellung](#91-aufgabenstellung)
- [10. Commit Messages - Introduction: Why good commit messages matter](#10-commit-messages---introduction-why-good-commit-messages-matter)
- [11. Hilfreiche Links](#11-hilfreiche-links)

# 1. Ziele
 - Sie haben ein GIT-Repository für Ihr persönliches Portfolio erstellt und mit einem Remote-Repository synchronisiert. 
 - Sie kennen die wichtigsten am häufigsten verwendeten Markdown Elemente
 - Sie kennen die wichtigsten GIT-Befehle und ihren Syntax (add, commit, push, pull, status, log).
 - Sie haben einen Quelletext-Editor ausgewählt, auf ihrem persönlichen Notebook eingerichtet und kennen die wichtigsten Bedienelemente. 

# 2. Eigenes Vorgehen planen - Wie erarbeite ich das Thema Git?
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeiten, Austausch in Gruppen |
| Aufgabenstellung  | Persönliches Lernplan erstellen für das neue Thema *Git* / Vorgehen planen |
| Zeitbudget  |  1 Lektion |
| Ziel | Jeder Lernende hat einen individuellen Lernplan für das Thema Git erstellt. <br>Mithilfe des Lernplanes kann der Lernende die Kompetenzen (Ziele) in der vorgegeben Zeit selbstständig erarbeiten. |

Die Abschnitte in diesem Dokument sind einzelne Aufträge die aufeinander aufgebaut sind. Demnach müssen die Arbeitsaufträge nacheinander gelöst werden und sollten nicht übersprungen werden. Als übergeordnetes Ziel richten Sie in diesem Kapitel ihr persönliches Portfolio für das Modul 231 ein, dass Teil der individuellen Leistungsbeurteilung ist. In der Beschreibung zur [Leistungsbeurteilung dieses Moduls 231](../00_evaluation/) finden Sie im Abschnitt *"LB2 - Persönliche Dossier"* zwei Bewertungskriterien (Kriterium 5 und 6), die Sie mit den Aufträgen in diesem Dokument erfüllen können. 

## 2.1. Aufgabe
Für diese Aufgabe benötigen Sie ein A4 Blatt und einen Stift. 
 1. Unterteilen Sie das A4 Blatt in drei Bereiche. Beschriften Sie diese mit: "Ziele", "Plan" und "Auswertung". 
 2. Listen Sie im Abschnitt "Ziele" die Kriterien 5 und 6 der Leistungsbeurteilung stichwortartig auf. 
 3. Lesen Sie alle Arbeitsaufträge in diesem Dokument aufmerksam durch.
 4. Überlegen Sie sich folgendes:<br> * Welche Arbeitsaufträge gehören zu welchem Kriterium?<br>* Was müssen Sie tun, um das Kriterium zu erfüllen?
 5. Wenn Sie ein Kriterium oder einen Arbeitsauftrag nicht verstehen, fragen Sie andere Lernende oder die Lehrperson. 
 6. Definieren Sie nun im Abschnitt "Plan" ihr Vorgehen:<br>Was machen Sie als erstes, als zweites, usw. ?<br>Welche Arbeitsaufträge möchten Sie heute in der Schule machen? Welche erarbeiten Sie zuhause? 
 7. Besprechen Sie Ihren Plan mit einem oder zwei Lernenden und optimieren Sie bei Bedarf Ihren Plan.
 8. Besprechen Sie den Plan mit der Lehrperson. 
 9. Gehen Sie gemäss Ihrem Plan vor und bearbeiten den nächsten Arbeitsauftrag. 


# 3. Versionsverwaltung mit GIT
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Einarbeitung in neues Thema |
| Zeitbudget  |  1 Lektion |
| Ziel | Sie wissen was *GIT* ist, wie es grundsätzlich funktioniert und weshalb es für Sie als Plattformentwickler ein wichtiges Tool ist.  |

## 3.1. Aufgabenstellung
Erreichen Sie das Ziel des Arbeitsauftrages indem Sie sich in das Thema Versionsverwaltung und GIT einlesen. Beantworten Sie anschliessend die Verständnisfragen. 

## 3.2. Versionsverwaltung in der Informatik
Versionsverwaltung ist heute in der Softwareentwicklung nicht mehr wegzudenken. Die manuelle Nachverfolgung von Änderungen und die mühsame Zusammenführung von Quellcode wurde früh als eine Aufgabe angesehen, die vom Computer erledigt werden soll. Einer der frühstens Versionsverwaltungssoftware für UNIX wurde 1972 in Bell Labs entwickelt. (In diesem Labor in der Nähe von New York wurden viele Durchbrüche in der Telekommunikationstechnik, Mathematik, Physik und Materialforschung erzielt.) 1982 folgte das *Revision Control System* (kurz RCS)[^1], gefolgt vom bereits heute noch bekannten *Concurrent Version Control* (kurz CVS). Bevor Linus Torvalds 2005 Git veröffentlichte dominierten andere Applikationen den Markt, wie Subversion, Perforce oder BitKeeper.

[^1]: <https://www.cs.purdue.edu/homes/trinkle/RCShome/>

Einige von diesen Systemen, insbesondere BitKeeper, würden sich für einen jungen Git User wie eine Zeitreise anfühlen. Versionskontrollsoftware vor Git funktionierte nach einem grundsätzlich anderen Paradigma. In einer Taxonomie[^2] von Eric Sink, Author von *Version Control by Example*, ist Git eine Versionskontrollsoftware der dritten Generation, während die meisten Vorgänger, die in den 1990er und 2000er populär waren, zur zweiten Generation angehören. Der wesentliche Unterschied besteht darin, dass die *2nd Generation* zentralisiert (centralized) und die *3rd generation version control systems* verteilt funktioniert.

[^2]: Einheitliches Verfahren oder Modell (Klassifikationsschema) mit dem Objekte nach bestimmen Kriterien klassifiziert werden.

Versionskontrolle ist nicht nur in der Softwareentwicklung praktisch. Applikationen aller Arten, wie Word, Excel, Photoshop, verfolgen die Änderungen des Benutzers und geben ihm die Möglichkeit diese Rückgängig zu machen. Filehosting-Dienste wie Dropbox, die es ermöglichen einen Ordner auf mehreren Geräten zu spiegeln müssen ebenfalls Versionskontrolle betreiben, indem Sie feststellen, wenn eine identische Datei auf zwei verschiedenen Geräten gleichzeitig verändert wurde.

In der Systemtechnik kommen mit Software-defined Infrastructure (kurz SDI oder) und *Infrastructure as Code* immer mehr komplexe und mächtige Konfigurationsdateien zum Einsatz. In einer einzigen Datei lassen sich ganze Netzwerktopologien oder Serverinfrastrukturen festlegen. Auch hier eignet sich Versionskontrollsoftware besonders gut, um Änderungen ohne grossen Aufwand nachverfolgen zu können und die Teamarbeit zu vereinfachen. Beispiele dafür findet man bei cloud-init[^3], vagrant oder docker[^4].

[^3]: <https://cloudinit.readthedocs.io/en/latest/>

[^4]: <https://docs.docker.com/compose/>

Das Ziel dieser Übung ist, dass Sie sich mit dem Thema Versionskontrolle auseinandersetzen und die Vorzüge kennenlernen. Wenn Sie mehr über die Geschichte der Versionskontrolle erfahren möchten, empfehle ich Ihnen diese beiden Links[^5]. Weitere interessante Informationen finden Sie hier[^6].

[^5]: <https://dev.to/thefern/history-of-version-control-systems-vcs-43ch> <https://twobithistory.org/2018/07/07/cvs.html>

[^6]: <https://bitbucket.org/product/de/version-control-software>

## 3.3. Was ist GIT?
![XKCD 1597](https://imgs.xkcd.com/comics/git.png)

GIT ist ein sogenanntes Versionskontrollsystem (VCS) und wurde Anfang 2005 von Linus Torvalds, dem Initiator des Linux-Kernels, entwickelt. Es erstaunt deshalb nicht, dass GIT konzeptionell ähnlich aufgebaut ist wie ein Linux-Filesystem.

Einen guten Einstieg in das Thema GIT erhalten Sie unter den folgenden Links:
 - https://github.com/ser-cal/M300-Git-Repo-Setup
 - https://dev.to/milu_franz/git-explained-the-basics-igc
 - https://hackernoon.com/understanding-git-fcffd87c15a3


## 3.4. Verständnisfragen
 - Was bedeutet die Abkürzung DVCS? 
 - Wie heisst das bekannteste DVCS System? 
 - Was ist der Unterschied zwischen einer zentralisierten und einer dezentralisierten Versionsverwaltung? 
 - Was bedeutet in diesem Zusammenhang die Abkürzung "MD"?
 - Weshalb ist "MD" in Zusammenspiel mit DVCS so nützlich?
 - Was ist wesentlich umfangreicher wie "MD" und wird sehr gerne für wissenschaftliche Arbeiten verwendet (z.B. Ihre zukünftige Bachelorarbeit an einer Hochschule)? 
 - Weshalb ist der Einsatz von docx Dateien mit dem bekanntesten DVCS System problematisch?


# 4. Eigenen Laptop für die Verwendung von GIT vorbereiten
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie wissen wie man GIT auf einem Notebook installiert.<br>Sie wissen wie man einen universalen Quelltext-Editor auf einem Notebook installiert. |

## 4.1. Anleitung (Windows 10)
 - Laden Sie sich von https://git-scm.com/ *Git für Windows* in der aktuellsten Version herunter und installieren Sie es auf Ihrem persönlichen Notebook. Lassen Sie bei der Installation alles auf *default*.
 - Laden Sie sich [Visual Studio Code](https://code.visualstudio.com/) herunter und installieren Sie es auf Ihrem persönlichen Notebook. Setzen Sie während der Installation die beiden Checkboxen "Add..." (siehe Screenshot):
![Visual Studio Installation](images/visualstudioinstallation_info.PNG)
 - Installieren Sie in Visual Studio Code die folgenden Plugins:
   - [Markdown Preview Enhanced](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)
   - [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
   - [Excel to Markdown table](https://marketplace.visualstudio.com/items?itemName=csholmq.excel-to-markdown-table)
   - [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker)
   - [German - Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-german)
<br>
![Visual Studio Code Menü](images/visualstudiocodemenu.PNG)

## 4.2. Tipps Visual Studio Code
 - Egal was sie machen möchten: Sie finden alles in der *Command pallete*. Der *default shortcut* ist ```Ctrl+Alt+P```<br>https://code.visualstudio.com/docs/getstarted/tips-and-tricks
 - **Markdown** (Plugins müssen installiert sein!):
   - Vorschau des aktuellen Markdown Dokumentes anzeigen:<br>1. ```Ctrl+Shift+P``` drücken<br>2. ```open preview``` tippen (Das ```>``` Symbol stehen lassen, sonst wird in den Dateisuchen-Modus gewechselt.) 3. Mit der Pfeiltaste den Eintrag *Markdown: Markdown Preview Enhanced: Open Preview to the side* auswählen und die *Enter*-Taste drücken.
   - Automatisch ein Inhaltsverzeichnis einfügen: *Command Pallete*: Der Befehl lautet *Markdown All in One: Create Table of Contents*
   - Automatisch Nummerieren: *Command Pallete*: *Markdown All in One: Add/Update section numbers*
   - Das Anfügen von ```<!-- omit in toc -->``` bewirkt, dass das entsprechende Heading nicht im Inhaltsverzeichnis aufgenommen wird und auch nicht nummeriert wird. 
![markdown omit in tok](images/markdown_omit.PNG)
 - Auf [Using Version Control in VS Code](https://code.visualstudio.com/docs/editor/versioncontrol) sind Erklärungen zu der *Source Control* Funktion von Visual Studio Code zu finden. Für Git müssen Sie keine zusätzlichen *Extensions* installieren.

# 5. Account auf gitlab mit TBZ E-Mail Adresse erstellen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie wissen wie Sie einen persönlichen GitLab-Account erstellen<br>Sie wissen wie man einen SSH-Key generiert.<br>Sie wissen wie Sie einen SSH Key auf GitLab hinterlegen können. |

**Wichtig: Für diesen Arbeitsauftrag müssen Sie auf Ihrem Notebook Git for Windows installiert haben!**

## 5.1. Anleitung
Eine ausführliche Anleitung zu SSH Keys erhalten Sie auf auf der GitLab Seite [GitLab and SSH keys](https://docs.gitlab.com/ee/ssh/). Die wichtigsten Schritte dabei sind: 

 - Erstellen Sie auf https://about.gitlab.com/ einen persönlichen GitLab-Account mit Ihrer TBZ E-Mail-Adresse. 
 - Öffnen Sie *Git Bash* und führen Sie folgenden Befehl aus, um Ihren persönlichen :
    ```bash
    ssh-keygen.exe -t ed25519  -C "IHRE E-MAIL ADRESSE"
    ```
    Dann werden sie gefragt unter welchem namen sie den Private Key speichern wollen. Indem sie die Enter-Taste drücken akzeptieren sie den default:
    ```
    Enter file in which to save the key (/Users/ado/.ssh/id_ed25519):
    ```
    Jetzt können sie noch ein Passwort zum Verschlüsseln ihres Private Keys wählen. Wenn sie einfach die Enter-Taste drücken wird der Key nicht verschlüsselt:
    ```
    Enter passphrase (empty for no passphrase): 
    Enter same passphrase again: 
    ```
    Dann kommt der folgende output der ihnen sagt wo welche Keys gespeichert sind:
    ```
    Your identification has been saved in /Users/ado/.ssh/id_ed25519
    Your public key has been saved in /Users/ado/.ssh/id_ed25519.pub
    The key fingerprint is:
    SHA256:c8+o8U0Y9HAgl7iPg9wbYL2ZI/sTFjzXrwb4OYGB/vE ado@Armins-Air.tbz.local
    The key's randomart image is:
    +--[ED25519 256]--+
    |        ..o.     |
    |        .o..     |
    |       + .o..    |
    |      + B..+.    |
    |     + +S&o ..   |
    |      = /o+*  .  |
    |       =.Xo++.   |
    |      . ++Eoo    |
    |       .o..o.    |
    +----[SHA256]-----+
    ```

 - Geben Sie den öffentlichen Teil ihres Keys auf Ihrem Terminal aus. Geben sie folgendes Kommando im Terminal ein:
    ```bash
    cat .ssh/id_ed25519.pub
    ```
    Dann kommt ungefähr folgender output:
    ```
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFIrO0nZsJXi4qLRUD93f7YNNHVL8bvbZk3oDxSMN+6a ado@Armins-Air.tbz.local
    ```
    Diesen output können sie verwenden um im Gitlab den SSH-Key zu hinterlegen.
 - Hinterlegen Sie den Key in Ihrem Gitlab Account: Siehe https://docs.gitlab.com/ee/ssh/#add-an-ssh-key-to-your-gitlab-account

 - Host-Key mit gitlab austauschen

    Git-Bash oeffnen und im terminal folgendes eingeben:
    ```
    git clone git@gitlab.com:ch-tbz-it/Stud/xxx.git
    ```
    Dann kommt folgendes
    ```
    Cloning into 'xxx'...
    The authenticity of host 'gitlab.com (172.65.251.78)' can't be established.
    ED25519 key fingerprint is SHA256:eUXGGm1YGsMAS7vkcx6JOJdOGHPem5gQp4taiCfCLB8.
    This key is not known by any other names
    Are you sure you want to continue connecting (yes/no/[fingerprint])? 
    ```
    Mit `yes` beantworten und den Rest der hier noch gezeigt ist ignorieren:
    ```
    Warning: Permanently added 'gitlab.com' (ED25519) to the list of known hosts.
    remote: 
    remote: ========================================================================
    remote: 
    remote: ERROR: The project you were looking for could not be found or you don't have permission to view it.

    remote: 
    remote: ========================================================================
    remote: 
    fatal: Could not read from remote repository.

    Please make sure you have the correct access rights
    and the repository exists.
    ```

- GIT-Config anpassen in Git-Bash:
    ```
    git config --global user.email "hans.muster@yzc.com"
    git config --global user.name "Hans Muster"
    ```
# 6. Neues Repository für persönliches Portfolio erstellen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziel | Sie wissen wie man ein Projekt auf gitlab erstellt und mit einem GIT Repository auf Ihrem lokalen PC verknüpft. |

Es gibt verschiedene Wege wie Sie ein neues Repository erstellen können. Grundsätzlich handelt es sich um einen Ordner auf Ihrem Computer den Sie mithilfe von GIT mit einem Server bzw. *Remote-Repository* synchronisieren können. Im Vergleich zu einem synchronisierten Ordner mit OneDrive, Google Drive, Dropbox oder ähnlich erfolgt die Synchronisierung nicht automatisch. 

## 6.1. Anleitung - Remote first
Diese Anleitung eignet sich, wenn Sie noch kein lokales Repository erstellt haben und ein neues Repository (z.B. Ihr Portfolio) anlegen möchten. 
 1. Loggen Sie sich in Ihren gitlab Account ein. (https://gitlab.com/)
 2. Gehen Sie auf das Dashboard (Gitlab Startseite https://gitlab.com/)
 3. Klicken Sie auf *New Project*
 4. Wählen Sie *Create blank project*
 5. Geben Sie dem Projekt einen Selbsterklärenden Namen (z.B. "M231 Portfolio")
 6. Wählen *Visibility Level: Private* und lassen Sie die Checkbox bei *Initialize repository with a README* gesetzt. 
 7. Klicken Sie auf *Create Project*
 8. Klicken Sie auf *Clone* und wählen Sie bei *Open in your IDE* *Visual Studio Code (SSH)*
![Open in Visual Studio Code SSH](images/gitlab_open_in_visualstudio.PNG)
 9. Nach einem Bestätigungsdialog von Visual Studio Code müssen Sie ein Verzeichnis wählen, in welches das Repository geklont werden soll. Empfehlenswert ist es im *Homefolder* einen Ordner *source* zu erstellen und dann diesen Ordner zu wählen. 
 ```C:\Users\yourusername\source\```
 10. Visual Studio Code klont nun das Repository und öffnet es in einem neuen *Workspace*. (Voraussetzung: Der SSH-Key ist korrekt hinterlegt.)

## 6.2. Aufbau der GitLab Repository Startseite
![](images/gitlab_repo_overview.png)

## 6.3. Repository Strukturierung
In der Strukturierung ihres Repositories sind sie grundsätzlich frei. Sie müssen jedoch folgendes beachten:
 - Der Aufbau und die Navigation müssen für den Leser intuitiv und ohne Anleitung bedienbar sein. 
 - Die Bewertungskriterien zum persönlichen Repository müssen erfüllt sein.

Als Vorlage für die Strukturierung ihres Repositories bzw. Portfolios können Sie [diese Vorlage](https://gitlab.com/alptbz/m231-portfolio) vorwenden. Passen Sie die Vorlage Ihren Bedürfnissen an. 

## 6.4. Anleitung für GitHub
Auf [dieser Webseite](https://github.com/ser-cal/M300-Git-Repo-Setup#git-lokal-einrichten-und-ein-erstes-repository-initialisieren-und-synchronisieren) finden Sie eine Anleitung wie Sie ein neues Repository initialisieren können. 

## 6.5. Anleitung -GitLab Project Member hinzufügen
![](images/gitlab_add_member.png)
Klicken Sie links in der Menüleiste auf *Project information* und anschliessend auf *Members*. 
![](images/gitlab_members.png)

## 6.6. Grundlagen Installation Visual Studio Code & SSH-Keys
In dem nachfolgenden Video wird gezeigt, wie Sie einen...
- GitLab Account anlegen, 
- ein GitLab Projekt erstellen,
- Visual Studio Code und Git installieren,
- ein SSH Schlüsselpaar erstellen und den Public-Key ihrem GitLab Account hinzufügen,
- zusätzliche Markdown Plugins in VS Code installieren,
- das Template in ihrem Projekt einfügen,
- den ersten Commit erstellen und pushen,
- E-Mail und Benutzername in der Git Konfiguration für Ihren lokalen Benutzeraccount festlegen.

![Video zu VS Code Git Markdown GitLab](videos/CodeGitMarkdown.webm)


## 6.7. Video - Installation von Git & Visual Studio, Neues Projekt erstellen, Projekt klonen
Im nachfolgenden Video wird folgendes gezeigt:
 - Installation von Visual Studio Code und Git
 - Erstellen eines Projektes auf GitLab
 - Klonen des Projektes auf den lokalen Computer

Im Video wird das Vorgehen bei zwei typischen Fehler gezeigt:
 - GitLab Host Key Verifikation
 - Hinterlegen der eigenen E-Mail und des Namens für die Signatur der Commits (**Hinterlegen Sie ihre eigenen Daten!**)

![Installation von Git & Visual Studio, Neues Projekt erstellen, Projekt klonen](videos/git_visual_studio_installation.webm)

# 7. Anleitungen
## 7.1. Git HTTPs durch SSH Connection String ersetzen
Im Video wird gezeigt, wie Sie mithilfe des Texteditors vim den Connection String ändern können. 

![Git - HTTPs durch SSH Connection String ersetzen](videos/Git_HTTPs_durch_SSH_Connection_String.webm)


# 8. Markdown Syntax kennenlernen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie sind in der Lage ein Dokument in Markdown zu gestalten.<br>Sie kennen die wichtigsten Markdown Elemente.<br> |

## 8.1. Aufgabenstellung
Gestalten Sie ein Markdown Dokument, dass gerendert genauso aussieht wie «Markdown Example.html». Informieren Sie sich dafür selbstständig über den Markdown Syntax. 

## 8.2. Links
 - https://www.markdownguide.org/basic-syntax/
 - https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

# 9. Git Branching
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie kennen die wichtigsten GIT-Befehle.<br>Sie haben ein grundsätzliches Verständnis von *commits*<br> |

Einige Git-Befehle werden Sie nur selten benötigen. Da Git sehr viel verwendet wird, kann fast jedes Problem *gegooglelt* werden und es gibt einen *stackoverflow*-Beitrag dazu.
![googling git cherry pick](images/google_git_issue.PNG)

Die wichtigsten Befehle und am häufigsten verwendeten Befehle (die man mit der Zeit auswendig kennt):
```bash
git clone <url> <folder> # clone remote repository in folder (omit folder name will use repository name as foldername)
git add <file> # stage one or multiple files
git commit # commit all staged files
git push # push all new commits to remote repository
git pull # pull all new commits from remote repository
git checkout <branch> # change branch
```

![git purr](images/git-purr.jpg)
**Sehr empfehlenswert:** Dieser Blog Artikel [GIT PURR! Git Command Explained with Cats!](https://girliemac.com/blog/2017/12/26/git-purr/) erklärt Git Commands mit Zeichnungen von Katzen. 

Es ist sehr hilfreich, wenn man die Grundlagen von *commits* und *branches* verstanden hat, damit man Potential von Git ausschöpfen kann. Eine gute Plattform dafür ist das nachfolgende spielerische Tool. 

## 9.1. Aufgabenstellung
Mithilfe des interaktiven Tutorials [Learn Git Branching](https://learngitbranching.js.org) lernen Sie spielerisch den Umgang mit GIT. Spielen Sie die einzelnen Levels durch. 

Empfohlene Levels:
 - Local: Introduction Sequence
 - Local: Ramping Up
 - Local: Moving Work Around
 - Local: A Mixed Bag
 - Remote: Push & Pull

# 10. Commit Messages - Introduction: Why good commit messages matter
![https://xkcd.com/1296/](https://imgs.xkcd.com/comics/git_commit.png)

Gut und knackig formulierte *commit messages* sind beim Einsatz von git zentral. Der Blog Artikel [Introduction: Why good commit messages matter](https://chris.beams.io/posts/git-commit/) erklärt die Problematik von schlechten *commit messages* und gibt Tipps für gute *commit messages*. 

# 11. Hilfreiche Links
 - [Bash cheatsheet](https://devhints.io/bash)
 - [Bash Crash Course](https://zach-gollwitzer.medium.com/the-ultimate-bash-crash-course-cb598141ad03)
 - [GIT PURR! im webarchiv](https://web.archive.org/web/20210918225036/https://girliemac.com/blog/2017/12/26/git-purr/)
 - [Git cheatsheet](https://www.atlassian.com/dam/jcr:e7e22f25-bba2-4ef1-a197-53f46b6df4a5/SWTM-2088_Atlassian-Git-Cheatsheet.pdf)
